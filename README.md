# wp-nijino-mori

## 環境

以下使える環境を用意してください

- docker
- docker-compose

## 準備

### .env を用意

.env.sample をコピーして .env を作成

以下は別途管理者に確認して追記してください

- DB のパスワード `PRODUCTION_DB_PASSWORD`
- SSH パスワード `PRODUCTION_SSH_PASSWORD`

## 本番環境から取得 ( Wordmove で pull)

Wordmove で本番環境と同期できます

```
$ docker exec -w /home/ -it wp-sun-green_wordmove wordmove pull --all
```

## 起動

docker-compose コマンドで起動

```
$ docker-compose up -d
```

この状態で http://localhost:8090 にアクセスすると WordPress が確認できます  
管理画面のユーザー名・パスワードは別途管理者に確認してください

## 本番環境に反映 ( Wordmove で push)

Wordmove で本番環境と同期できます

```
$ docker exec -w /home/ -it wp-nijino-mori_wordmove wordmove push --all
```

オプションを変更してテーマだけなども可能です

[Wordmove の基本操作 \- Qiita](https://qiita.com/mrymmh/items/c644934cac386d95b7df)

## テーマの開発

テーマフォルダに移動して npm install

```
$ cd public/wp-content/themes/nijino-mori
$ npm install
```

gulp コマンドを実行すると、localhost:3001 でサイトが起動

```
$ npx gulp
```

#### SCSS

stylelint を使ってフォーマットやエラーチェックを実施している

### テスト

#### スクリーンショットで確認

テーマフォルダ内の test でスクリーンショットを撮って各サイズの確認ができる

```
$ cd public/wp-content/themes/sun-green/test
$ npm install
$ npm run test
//テストが実行されてスクリーンショットが保存される
$  ls images/result
index_pc.png            index_sp.png            module_pc.png           module_sp.png
index_pc_max.png        index_sp_min.png        module_pc_max.png       module_sp_min.png
index_pc_min.png        index_tab.png           module_pc_min.png       module_tab.png
```

とりあえず保存されるだけ

## 初期化

すべてやり直したい場合は以下コマンドで消せます

```
$ docker-compose down --rmi all --volumes --remove-orphans
```

[《滅びの呪文》Docker Compose で作ったコンテナ、イメージ、ボリューム、ネットワークを一括完全消去する便利コマンド \- Qiita](https://qiita.com/suin/items/19d65e191b96a0079417)
